﻿using Spire.Pdf;
using Spire.Pdf.Graphics;
using Spire.Pdf.HtmlConverter;
using Spire.Pdf.HtmlConverter.Qt;
using System;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Web.Http;



namespace MicroServices.Controllers
{
    public class PdfController : ApiController
    {
        
 
        public IHttpActionResult Create([FromBody] PrintInfo printInfo)
        {
            if (printInfo == null) throw new ArgumentNullException(nameof(printInfo));

            var result = CreatePdf(printInfo.Content, "download");

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new ByteArrayContent(result.File);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue(result.ContentType);
            response.Content.Headers.ContentLength = result.File.Length;
            response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = result.FileName
            };

            return ResponseMessage(response);
        }

        protected FileResultInfo CreatePdf(string content, string title)
        {
            if (content == null) throw new ArgumentNullException(nameof(content));


            PdfDocument pdf = new PdfDocument();
            PdfHtmlLayoutFormat htmlLayoutFormat = new PdfHtmlLayoutFormat();
            htmlLayoutFormat.IsWaiting = false;
            PdfPageSettings setting = new PdfPageSettings();
            setting.Size = PdfPageSize.A4;
            string htmlCode = content;      


            using (var stream = new MemoryStream())
            {
                if(content.Substring(0,4) == "http")
                {
                    Spire.Pdf.HtmlConverter.Qt.HtmlConverter.Convert(content, stream, true, 10 * 1000, new SizeF(590, 834), new PdfMargins(0));
                }
                else
                {
                    Spire.Pdf.HtmlConverter.Qt.HtmlConverter.Convert(content, stream, true, 10 * 1000, new SizeF(590, 834), new PdfMargins(0), Spire.Pdf.HtmlConverter.LoadHtmlType.SourceCode);
                }

                stream.Flush();
                stream.Position = 0;

                return new FileResultInfo
                {
                    File = stream.ToArray(),
                    ContentType = "application/octet-stream",
                    FileName = $"{title}.pdf"
                };
            }
        }
    }
    
    public class FileResultInfo
    {
        public byte[] File { get; set; }
        public string ContentType { get; set; }
        public string FileName { get; set; }
    }

    public class PrintInfo
    {
        public string Content { get; set; }
    }
}
